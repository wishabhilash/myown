
require.config({
	shim: {
		bootstrap: {
			deps: ['jquery']
		},
	},

	paths: {
		jquery: './lib/jquery',
		can: './lib/canjs/can',
		bootstrap: './lib/bootstrap.min',
	},

})

require([
	"jquery",
	'can',
	'can/route/pushstate',
	'bootstrap',
], function($, can){
	'use strict';

	$(function(){
		can.route.ready();
		can.route("/:sidebarmenu", {tabname: "frameworks"});
	})

})