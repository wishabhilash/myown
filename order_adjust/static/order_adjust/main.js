
require.config({
	shim: {
		bootstrap: {
			deps: ['jquery']
		},
	},

	paths: {
		jquery: './lib/jquery',
		can: './lib/canjs/can',
		bootstrap: './lib/bootstrap/bootstrap.min',
	},

})

require([
	"jquery",
	'can',
	'bootstrap',
	'./app/calculate'
], function($, can){
	'use strict';

	$(function(){
		can.route.ready();
		can.route("/:sidebarmenu", {tabname: "frameworks"});

		$("#container").html(can.view("core-template", {}));
	})

})