define([
	'jquery',
	'can',
],function($, can){
	'use strict';

	return can.Component.extend({
		tag: 'calculate-app',
		template: "",
		scope: {
			init: function(){
                this.max_discount = can.compute(200);
                this.orders = can.compute(0);
                this.gross_amount = can.compute(0);
                this.gross_discount = can.compute(0);

                this.changed_gross_amount = can.compute(0);
                this.changed_gross_discount = can.compute(0);

                this.cleaning_type = can.compute('dryclean');

                this.distribution = new can.Map({
                	'190': {'percent': 30, 'orders': 0},
                	'140': {'percent': 10, 'orders': 0},
                	'60': {'percent': 30, 'orders': 0},
                	'140-60': {'percent': 10, 'orders': 0},
                	'190-60': {'percent': 0, 'orders': 0},
                })
			},

			orderRowData: new can.List([]),

			getMaxDiscount: function(){
				return this.max_discount();
			},

			getOrdersNum: function(){
				return this.orders();
			},

			getGrossAmount: function(){
				return this.gross_amount();
			},

			getGrossDiscount: function(){
				return this.gross_discount();
			},

			getChangedGrossAmount: function(){
				return this.changed_gross_amount();
			},

			getChangedGrossDiscount: function(){
				return this.changed_gross_discount();
			},

			getCleaningType: function(){
				return this.cleaning_type();
			},

			maxDiscountChange: function(context, element, event){
				this.max_discount(parseInt(element.val()));
			},

			ordersChange: function(context, element, event){
				this.orders(parseInt(element.val()));
			},

			grossAmountChange: function(context, element, event){
				this.gross_amount(parseInt(element.val()));
			},

			grossDiscountChange: function(context, element, event){
				this.gross_discount(parseInt(element.val()));
			},

			cleaningTypeChange: function(context, element, event){
				this.cleaning_type(element.val());
			},

			_get_item_amount: function(item){
				return item.attr('190') * 190 + item.attr('140') * 140 + item.attr('60') * 60;
			},

			_get_item_amount_laundry: function(item){
				return item.attr('19') * 19 + item.attr('50') * 50 + item.attr('60') * 60 + item.attr('90') * 90;
			},

			_190Change: function(context, element, event){
				var index = parseInt(element.attr('index'));
				var item = this.orderRowData.attr(index);
				var value = parseInt(element.val());
				item.attr('190', value);
				var temp_amount = this._get_item_amount(item);
				item.attr('amount', temp_amount);
				var temp_discount = item.attr('amount')/2;
				if (temp_discount > this.max_discount()) temp_discount = this.max_discount();
				item.attr('discount', temp_discount);
				this._update_all_data();
			},

			_140Change: function(context, element, event){
				var index = parseInt(element.attr('index'));
				var item = this.orderRowData.attr(index);
				var value = parseInt(element.val());
				item.attr('140', value);
				var temp_amount = this._get_item_amount(item);
				item.attr('amount', temp_amount);
				var temp_discount = item.attr('amount')/2;
				if (temp_discount > this.max_discount()) temp_discount = this.max_discount();
				item.attr('discount', temp_discount);
				this._update_all_data();
			},

			_60Change: function(context, element, event){
				var index = parseInt(element.attr('index'));
				var item = this.orderRowData.attr(index);
				var value = parseInt(element.val());
				item.attr('60', value);
				var temp_amount = this._get_item_amount(item);
				item.attr('amount', temp_amount);
				var temp_discount = item.attr('amount')/2;
				if (temp_discount > this.max_discount()) temp_discount = this.max_discount();
				item.attr('discount', temp_discount);
				this._update_all_data();
			},

			_60LaundryChange: function(context, element, event){
				var index = parseInt(element.attr('index'));
				var item = this.orderRowData.attr(index);
				var value = parseInt(element.val());
				item.attr('60', value);
				var temp_amount = this._get_item_amount_laundry(item);
				item.attr('amount', temp_amount);
				var temp_discount = item.attr('amount')/2;
				if (temp_discount > this.max_discount()) temp_discount = this.max_discount();
				item.attr('discount', temp_discount);
				this._update_all_data();
			},

			_19Change: function(context, element, event){
				var index = parseInt(element.attr('index'));
				var item = this.orderRowData.attr(index);
				var value = parseInt(element.val());
				item.attr('19', value);
				var temp_amount = this._get_item_amount_laundry(item);
				item.attr('amount', temp_amount);
				var temp_discount = item.attr('amount')/2;
				if (temp_discount > this.max_discount()) temp_discount = this.max_discount();
				item.attr('discount', temp_discount);
				this._update_all_data();
			},

			_50Change: function(context, element, event){
				var index = parseInt(element.attr('index'));
				var item = this.orderRowData.attr(index);
				var value = parseInt(element.val());
				item.attr('50', value);
				var temp_amount = this._get_item_amount_laundry(item);
				item.attr('amount', temp_amount);
				var temp_discount = item.attr('amount')/2;
				if (temp_discount > this.max_discount()) temp_discount = this.max_discount();
				item.attr('discount', temp_discount);
				this._update_all_data();
			},

			_90Change: function(context, element, event){
				var index = parseInt(element.attr('index'));
				var item = this.orderRowData.attr(index);
				var value = parseInt(element.val());
				item.attr('90', value);
				var temp_amount = this._get_item_amount_laundry(item);
				item.attr('amount', temp_amount);
				var temp_discount = item.attr('amount')/2;
				if (temp_discount > this.max_discount()) temp_discount = this.max_discount();
				item.attr('discount', temp_discount);
				this._update_all_data();
			},

			startCalculation: function(context, element, event){
				var self = this;
				this._update_distribution();
				this._fill_data();
				this._update_all_data();
			},

			_gen_random: function(min, max){
				if (typeof max == 'undefined'){
					max = min;
					min = 1
				}
				return Math.floor(Math.random() * (max - min) + min);
			},

			_get_dryclean_template: function(){
				return {'190': 0, '140': 0, '60': 0, 'amount': 0, 'discount': 0};
			},

			_get_laundry_template: function(){
				return {'19': 0, '60': 0, '50': 0, '90': 0, 'amount': 0, 'discount': 0};
			},

			__fill_data_dryclean: function(){
				var self = this;
				var final_list = [];
				this.distribution.each(function(value, key){
					if (key == '190') {
						for (var i = 0; i < value['orders']; i++) {
							var template = self._get_dryclean_template();
							template['190'] += self._gen_random(3);
							template['amount'] = template['190'] * 190;
							template['discount'] = template['amount']/2;
							if (template['discount'] > self.max_discount()) template['discount'] = self.max_discount();
							final_list.push(template)
						};
					} else if(key == '140') {
						for (var i = 0; i < value['orders']; i++) {
							var template = self._get_dryclean_template();
							template['140'] += self._gen_random(2);
							template['amount'] = template['140'] * 140;
							template['discount'] = template['amount']/2;
							if (template['discount'] > self.max_discount()) template['discount'] = self.max_discount();
							final_list.push(template)
						};
					} else if(key == '60') {
						for (var i = 0; i < value['orders']; i++) {
							var template = self._get_dryclean_template();
							template['60'] += self._gen_random(3);
							template['amount'] = template['60'] * 60;
							template['discount'] = template['amount']/2;
							if (template['discount'] > self.max_discount()) template['discount'] = self.max_discount();
							final_list.push(template)
						};
					} else if(key == '140-60') {
						for (var i = 0; i < value['orders']; i++) {
							var template = self._get_dryclean_template();
							template['140'] += self._gen_random(2);
							template['60'] += self._gen_random(3);
							template['amount'] = (template['140']*140) + (template['60']*60);
							template['discount'] = template['amount']/2;
							if (template['discount'] > self.max_discount()) template['discount'] = self.max_discount();
							final_list.push(template)
						};
					} else if(key == '190-60') {
						for (var i = 0; i < value['orders']; i++) {
							var template = self._get_dryclean_template();
							template['190'] += self._gen_random(4);
							template['60'] += self._gen_random(5);
							template['amount'] = (template['190']*190) + (template['60']*60);
							template['discount'] = template['amount']/2;
							if (template['discount'] > self.max_discount()) template['discount'] = self.max_discount();
							final_list.push(template)
						};
					}
				})
				this.orderRowData.replace(final_list);
			},

			__fill_data_laundry: function(){
				var self = this;
				var final_list = [];
				this.distribution.each(function(value, key){
					if (key == '19') {
						for (var i = 0; i < value['orders']; i++) {
							var template = self._get_laundry_template();
							template['19'] += self._gen_random(2, 7);
							template['amount'] = template['19'] * 19;
							template['discount'] = Math.floor(template['amount']/2);
							if (template['discount'] > self.max_discount()) template['discount'] = self.max_discount();
							final_list.push(template)
						};
					} else if(key == '60') {
						for (var i = 0; i < value['orders']; i++) {
							var template = self._get_laundry_template();
							template['60'] += self._gen_random(2);
							template['amount'] = template['60'] * 60;
							template['discount'] = Math.floor(template['amount']/2);
							if (template['discount'] > self.max_discount()) template['discount'] = self.max_discount();
							final_list.push(template)
						};
					} else if(key == '90') {
						for (var i = 0; i < value['orders']; i++) {
							var template = self._get_laundry_template();
							template['90'] += self._gen_random(2);
							template['amount'] = template['90'] * 90;
							template['discount'] = Math.floor(template['amount']/2);
							if (template['discount'] > self.max_discount()) template['discount'] = self.max_discount();
							final_list.push(template)
						};
					} else if(key == '50') {
						for (var i = 0; i < value['orders']; i++) {
							var template = self._get_laundry_template();
							template['50'] += self._gen_random(2);
							template['amount'] = template['50'] * 50;
							template['discount'] = Math.floor(template['amount']/2);
							if (template['discount'] > self.max_discount()) template['discount'] = self.max_discount();
							final_list.push(template)
						};
					} else if(key == '19-50') {
						for (var i = 0; i < value['orders']; i++) {
							var template = self._get_laundry_template();
							template['19'] += self._gen_random(5);
							template['50'] += self._gen_random(2);
							template['amount'] = (template['19']*19) + (template['50']*50);
							template['discount'] = Math.floor(template['amount']/2);
							if (template['discount'] > self.max_discount()) template['discount'] = self.max_discount();
							final_list.push(template)
						};
					} else if(key == '19-60') {
						for (var i = 0; i < value['orders']; i++) {
							var template = self._get_laundry_template();
							template['19'] += self._gen_random(5);
							template['60'] += self._gen_random(2);
							template['amount'] = (template['19']*19) + (template['60']*60);
							template['discount'] = Math.floor(template['amount']/2);
							if (template['discount'] > self.max_discount()) template['discount'] = self.max_discount();
							final_list.push(template)
						};
					} else if(key == '19-60-50') {
						for (var i = 0; i < value['orders']; i++) {
							var template = self._get_laundry_template();
							template['19'] += self._gen_random(40);
							template['60'] += self._gen_random(20);
							template['50'] += self._gen_random(25);
							template['amount'] = (template['19']*19) + (template['60']*60) + (template['50']*50);
							template['discount'] = Math.floor(template['amount']/2);
							if (template['discount'] > self.max_discount()) template['discount'] = self.max_discount();
							final_list.push(template)
						};
					}
				})
				this.orderRowData.replace(final_list);
			},

			_fill_data: function(){
				if (this.cleaning_type() == 'dryclean') {
					this.__fill_data_dryclean();
				} else if (this.cleaning_type() == 'laundry') {
					this.__fill_data_laundry();
				} else if (this.cleaning_type() == 'iron') {
					this.__fill_data_iron();
				};
			},

			_update_distribution: function(){
				var self = this;
				if (this.cleaning_type() == 'dryclean') {
					var _dic = {
	                	'190': {'percent': 30, 'orders': 0},
	                	'140': {'percent': 10, 'orders': 0},
	                	'60': {'percent': 25, 'orders': 0},
	                	'140-60': {'percent': 15, 'orders': 0},
	                	'190-60': {'percent': 0, 'orders': 0},
	                }
	                var temp_orders = 0;
	                $.each(_dic, function(key, value){
	                	if (key == '190-60') {
	                		value['orders'] = self.orders() - temp_orders;
	                	}else{
	                		value['orders'] = Math.floor(self.orders()*value['percent']/100)
	                		temp_orders += value['orders']
	                	}
	                })
	                this.distribution.attr(_dic, true)

				} else if (this.cleaning_type() == 'laundry') {
					var _dic = {
	                	'19': {'percent': 30, 'orders': 0},
	                	'60': {'percent': 10, 'orders': 0},
	                	'50': {'percent': 10, 'orders': 0},
	                	'90': {'percent': 5, 'orders': 0},
	                	'19-50': {'percent': 20, 'orders': 0},
	                	'19-60': {'percent': 15, 'orders': 0},
	                	'19-60-50': {'percent': 0, 'orders': 0},
	                }
	                var temp_orders = 0;
	                $.each(_dic, function(key, value){
	                	if (key == '19-60-50') {
	                		value['orders'] = self.orders() - temp_orders;
	                	}else{
	                		value['orders'] = Math.floor(self.orders()*value['percent']/100)
	                		temp_orders += value['orders']
	                	}
	                })
	                this.distribution.attr(_dic, true)
				} else if (this.cleaning_type() == 'iron') {

				}
			},

			_update_all_data: function(){
				var temp_amount = 0, temp_discount = 0;
				this.orderRowData.each(function(value, key){
					temp_amount += value['amount'];
					temp_discount += value['discount'];
				})

				this.changed_gross_amount(temp_amount);
				this.changed_gross_discount(temp_discount);
			},

			downloadCSV: function(context, element, event){
				var cleaning_type = this.cleaning_type();
				var all_data_raw = $('#' + cleaning_type + "-data .row");
				var all_data = ""
				if (cleaning_type == 'dryclean') {
					all_data += '<div>Tops/Bottoms/Kurta/Salwar, Jacket/Hoodie/Bedsheet/Doormat/Soft toys, Blazer/Saree/Curtain, Amount, Discount</div>';
					$.each(all_data_raw, function(key, value){
						var item = [];
						item.push(parseInt($(value).find("._60").val()));
						item.push(parseInt($(value).find("._140").val()));
						item.push(parseInt($(value).find("._190").val()));
						item.push(parseInt($(value).find(".amount").text()));
						item.push(parseInt($(value).find(".discount").text()));
						all_data += '<div>' + item.join() + '</div>'
					})
				} else if (cleaning_type == 'laundry') {
					all_data += '<div>Tops/Bottoms/Kurta/Salwar, Curtains/Pillow/Sofa cover/Bedsheet, Jacket/Hoodie/Saree, Amount, Discount</div>';
					$.each(all_data_raw, function(key, value){
						var item = [];
						item.push(parseInt($(value).find("._19").val()));
						item.push(parseInt($(value).find("._50").val()));
						item.push(parseInt($(value).find("._60").val()));
						item.push(parseInt($(value).find(".amount").text()));
						item.push(parseInt($(value).find(".discount").text()));
						all_data += '<div>' + item.join() + '</div>'
					})
				};

				var win = window.open('about:blank');
				win.document.write(all_data);
				win.document.close();
				
				// $.ajax({
				// 	url: "/download_csv",
				// 	type: 'post',
				// 	data: {
				// 		all_data: JSON.stringify(all_data),
				// 		cleaning_type: cleaning_type
				// 	},
				// 	success: function(data){
						
				// 	}
				// })
			}

		},

		helpers: {
			ifEqual: function(a, b ,options){
				var fnTrue = options.fn, fnFalse = options.inverse;
				if (typeof a == "function") {
					a = a();
				}

				if (typeof b == "function") {
					b = b();
				}
				return a == b ? fnTrue() : fnFalse();
			},
		},

		events: {
			'inserted': function(){

			}
		}

	})

})