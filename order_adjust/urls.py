from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'order_adjust.views.index', name='index'),
    url(r'^download_csv$', 'order_adjust.views.download_csv', name='download_csv'),

    # url(r'^admin/', include(admin.site.urls)),
)
