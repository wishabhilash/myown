from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
import json
import csv



dryclean_items = {
    '60': "Tops/Bottoms/Kurta/Salwar",
    '140': "Jacket/Hoodie/Bedsheet/Doormat/Soft toys",
    '190': "Blazer/Saree/Curtain"
}

laundry_items = {
    '19': "Tops/Bottoms/Kurta/Salwar",
    '50': "Curtains/Pillow/Sofa cover/Bedsheet",
    '60': "Jacket/Hoodie/Saree",
}


def index(request):
    return render(request, "base.html")


@csrf_exempt
def download_csv(request):
    cleaning_type = request.POST['cleaning_type']
    data = json.loads(request.POST['all_data'])
    response = HttpResponse(content_type='text/csv')
    writer = csv.writer(response)

    if cleaning_type == 'laundry':
        pass
    elif cleaning_type == 'dryclean':
        writer.writerow([
            dryclean_items['60'],
            dryclean_items['140'],
            dryclean_items['190'],
            'amount',
            'discount'
        ])
        for data_item in data:
            writer.writerow([
                data_item['60'],
                data_item['140'],
                data_item['190'],
                data_item['amount'],
                data_item['discount']
            ])
    return response
